<?php return array(
    'root' => array(
        'name' => 'wpsyntex/polylang-wc',
        'pretty_version' => '2.1.x-dev',
        'version' => '2.1.9999999.9999999-dev',
        'reference' => '020d5bbc467500a3a5df26574d9f9bc8efb21c5a',
        'type' => 'wordpress-plugin',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => false,
    ),
    'versions' => array(
        'wpsyntex/polylang-wc' => array(
            'pretty_version' => '2.1.x-dev',
            'version' => '2.1.9999999.9999999-dev',
            'reference' => '020d5bbc467500a3a5df26574d9f9bc8efb21c5a',
            'type' => 'wordpress-plugin',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
